python-saharaclient (4.2.0-4) unstable; urgency=medium

  * Removed python3-pep8 from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 14 Jan 2025 09:23:41 +0100

python-saharaclient (4.2.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090611).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 10:14:19 +0100

python-saharaclient (4.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 16:57:34 +0200

python-saharaclient (4.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 Sep 2023 14:44:11 +0200

python-saharaclient (4.1.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1046307).

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Aug 2023 23:51:41 +0200

python-saharaclient (4.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 13:23:06 +0200

python-saharaclient (4.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Feb 2023 09:45:01 +0100

python-saharaclient (4.0.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 14:41:46 +0200

python-saharaclient (4.0.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Sep 2022 11:45:14 +0200

python-saharaclient (4.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 Sep 2022 14:14:10 +0200

python-saharaclient (3.5.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 11:35:00 +0100

python-saharaclient (3.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Feb 2022 09:25:10 +0100

python-saharaclient (3.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 17:52:16 +0200

python-saharaclient (3.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 20 Sep 2021 15:48:24 +0200

python-saharaclient (3.3.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 11:51:53 +0200

python-saharaclient (3.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Debhelper 11.
  * Standards-Version: 4.5.1.

 -- Thomas Goirand <zigo@debian.org>  Sun, 14 Mar 2021 15:54:39 +0100

python-saharaclient (3.2.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2020 17:13:50 +0200

python-saharaclient (3.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-babel from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Sep 2020 22:39:24 +0200

python-saharaclient (3.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-mock from build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Sep 2020 12:24:06 +0200

python-saharaclient (3.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 23:34:58 +0200

python-saharaclient (3.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 09 Apr 2020 21:21:52 +0200

python-saharaclient (2.3.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 01:46:37 +0200

python-saharaclient (2.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 09:15:45 +0200

python-saharaclient (2.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 14:27:08 +0200

python-saharaclient (2.2.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs

  [ Thomas Goirand ]
  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Mar 2019 22:21:36 +0100

python-saharaclient (2.0.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Remove obsolete alternatives on upgrade.  (Closes: #910224)

 -- Andreas Beckmann <anbe@debian.org>  Mon, 11 Mar 2019 12:08:08 +0100

python-saharaclient (2.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Sep 2018 22:45:05 +0200

python-saharaclient (2.0.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Building sphinx doc with Python 3.
  * Removed fix-sphinx-Directive-import.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 13:34:53 +0200

python-saharaclient (1.5.0-4) unstable; urgency=medium

  * Add patch to fix sphinx Directive module import (Closes: #896634).

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Apr 2018 10:28:30 +0200

python-saharaclient (1.5.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:55:42 +0000

python-saharaclient (1.5.0-2) experimental; urgency=medium

  * Fixed minimum version of openstack-pkg-tools to 66~ to support stestr,
    as this is what saharaclient uses now (Closes: #890856).
  * Bumped debhelper to 10.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Feb 2018 07:30:38 +0000

python-saharaclient (1.5.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Feb 2018 21:32:30 +0000

python-saharaclient (1.3.0-4) unstable; urgency=medium

  * Add missing build-depends on python3-hacking (Closes: #880860).

 -- Thomas Goirand <zigo@debian.org>  Sun, 05 Nov 2017 12:50:29 +0000

python-saharaclient (1.3.0-3) unstable; urgency=medium

  * Build-depends on python{3,}-pep8 (Closes: #880179).
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Oct 2017 18:16:59 +0000

python-saharaclient (1.3.0-2) experimental; urgency=medium

  * Added Python 3 support.
  * Reviewed d/copyright holders.
  * Handle the <!nodoc> build profile.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2017 11:21:12 +0200

python-saharaclient (1.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/copyright: Changed source URL to https protocol

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed remove-failing-unit-discovery.patch, files are gone upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Oct 2017 23:24:44 +0200

python-saharaclient (0.11.1-3) unstable; urgency=medium

  * Removed files failing unit test discovery with newer novaclient:
    - saharaclient/tests/integration/tests/test_vanilla.py
    - saharaclient/tests/integration/tests/test_vanilla2.py
    - saharaclient/tests/integration/tests/test_hdp.py

 -- Thomas Goirand <zigo@debian.org>  Mon, 25 Jan 2016 18:10:33 +0000

python-saharaclient (0.11.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 22:34:12 +0000

python-saharaclient (0.11.1-1) experimental; urgency=medium

  * New upstream release.
  * d/control: Align dependencies and versions with upstream.
  * d/control: Update uploaders.

 -- Corey Bryant <corey.bryant@canonical.com>  Thu, 24 Sep 2015 14:36:59 -0400

python-saharaclient (0.10.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Sep 2015 14:15:42 +0200

python-saharaclient (0.9.1-1) experimental; urgency=medium

  * Team upload.
  * New upstream release:
    - d/control: Align dependencies and versions with upstream.

 -- James Page <james.page@ubuntu.com>  Tue, 23 Jun 2015 11:54:41 +0100

python-saharaclient (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Reviewed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Dec 2014 23:23:00 +0800

python-saharaclient (0.7.3-1) experimental; urgency=medium

  * New upstream release.
  * Uploading to experimental before the Jessie freeze.
  * Updated (build-)depends for this release.
  * Allow the unit tests to fail, because it isn't desirable to build with the
    keystone server up, which is what the unit tests are expecting.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Sep 2014 23:08:48 +0800

python-saharaclient (0.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Apr 2014 07:38:47 +0000

python-saharaclient (0.6.0-1) experimental; urgency=low

  * Initial release (Closes: #742478).

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Mar 2014 14:58:19 +0800
